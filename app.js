/* eslint-disable no-shadow */
const fs = require("fs");
const path = require("path");
const { exec } = require("child_process");

(async () => {
  const carpetaOrigen = ".";
  let carpetaDestino;

  // Comprobamos si nos han pasado el nombre de la nueva carpeta
  if (!process.argv[2]) {
    console.log("No se ha proporcionado el nombre de la nueva carpeta");
    process.exit(1);
  } else {
    carpetaDestino = process.argv[2];
  }
  const pathOrigen = path.resolve(carpetaOrigen);
  const pathDestino = path.resolve("..", carpetaDestino);

  // Comprobamos si existe la carpeta origen
  if (!fs.existsSync(pathOrigen)) {
    console.log(`No existe la carpeta origen ${carpetaOrigen}`);
    process.exit(1);
  }

  // Creamos la carpeta destino
  fs.mkdir(pathDestino, err => {
    if (err) {
      console.log(`No se ha podido crear la carpeta "${carpetaDestino}", comprueba que no exista ya una carpeta con ese nombre`);
      process.exit(1);
    }
    console.log(`Se ha creado la carpeta "${carpetaDestino}"`);
    copiar();
    instalar();
  });

  function instalar() {
    process.chdir(pathDestino);
    console.log("Generando package.json");
    exec("npm init -y", (error, stdout, stderr) => {
      console.log(stdout);
      console.log("Instalando paquetes de ESLint");
      exec("npm i -D eslint eslint-config-airbnb-base eslint-plugin-import", (err, stdout, stderr) => {
        console.log(stdout);
        console.log("\nOperaciones finalizadas");
      });
    });
  }

  function copiar() {
    // Copiamos los archivos
    console.log("Copiando archivos");
    const archivos = [".editorconfig", ".eslintrc.js"];
    for (const archivo of archivos) {
      fs.copyFileSync(path.join(pathOrigen, archivo), path.join(pathDestino, archivo));
    }
  }
})();
